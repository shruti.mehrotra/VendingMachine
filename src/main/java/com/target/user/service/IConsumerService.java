package com.target.user.service;

import java.util.List;

import com.target.model.Cash;
import com.target.model.response.ServiceResponse;
import com.target.user.request.InventoryRequest;
import com.target.user.request.UpdateInventoryRequest;

public interface IConsumerService {
	public ServiceResponse selectProducts(InventoryRequest request);
	
	public ServiceResponse updateInventory(UpdateInventoryRequest request);
	
	public ServiceResponse acceptPayment(List<Cash> paymentList);
	
	public ServiceResponse completeTransaction(boolean completeTxn);
	
	public ServiceResponse cancelRequest(InventoryRequest request);
	
	public ServiceResponse takeRefund(List<Cash> refundList);
}
