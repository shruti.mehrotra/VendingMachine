package com.target.user.request;

import com.target.model.Inventory;

import lombok.Data;

@Data
public class UpdateInventoryRequest {
	Inventory inventory;
}
