package com.target.user.request;

import com.target.model.Inventory;

import lombok.Data;

@Data
public class InventoryRequest {
	Inventory inventory;
}
