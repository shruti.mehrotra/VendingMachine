package com.target.admin.response;

import java.util.List;

import com.target.model.Cash;
import com.target.model.Inventory;
import com.target.model.response.ServiceResponse;

import lombok.Data;

@Data
public class SnapshotResponse extends ServiceResponse {
	List<Inventory> inventoryList;
	List<Cash> cashList;
	Long totalCash;
}
