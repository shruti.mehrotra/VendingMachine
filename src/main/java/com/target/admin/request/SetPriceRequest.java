package com.target.admin.request;

import lombok.Data;

@Data
public class SetPriceRequest {
	private String productId;
	private Integer price;
}
