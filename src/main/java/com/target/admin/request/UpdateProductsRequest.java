package com.target.admin.request;

import com.target.model.Inventory;

import lombok.Data;

@Data
public class UpdateProductsRequest {
	Inventory inventory;
}
