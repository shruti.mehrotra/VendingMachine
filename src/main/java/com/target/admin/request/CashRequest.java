package com.target.admin.request;

import java.util.List;

import com.target.model.Cash;

import lombok.Data;

@Data
public class CashRequest {
	List<Cash> cashList;
}
