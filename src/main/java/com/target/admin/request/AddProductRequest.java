package com.target.admin.request;

import com.target.model.Product;

import lombok.Data;

@Data
public class AddProductRequest {
	Product product;
}
