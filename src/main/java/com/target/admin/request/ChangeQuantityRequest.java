package com.target.admin.request;

import lombok.Data;

@Data
public class ChangeQuantityRequest {
	private String productId;
	private Integer quantity;
}
