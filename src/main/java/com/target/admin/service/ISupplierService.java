package com.target.admin.service;

import java.util.List;

import com.target.admin.request.AddProductRequest;
import com.target.admin.request.CashRequest;
import com.target.admin.request.ChangeQuantityRequest;
import com.target.admin.request.SetPriceRequest;
import com.target.admin.response.SnapshotResponse;
import com.target.model.Cash;
import com.target.model.Inventory;
import com.target.model.response.ServiceResponse;

public interface ISupplierService {
	public ServiceResponse addProductToInventory(AddProductRequest request, Inventory inventory);
	
	public ServiceResponse resetInventory(Inventory inventory);

	ServiceResponse changeQuantity(ChangeQuantityRequest request, Inventory inventory);

	ServiceResponse setPrice(SetPriceRequest request, Inventory inventory);

	SnapshotResponse collectCash(CashRequest request, List<Cash> cashList);

	SnapshotResponse addCash(CashRequest request, List<Cash> cashList);

	ServiceResponse resetCash(List<Cash> cashList);
}
