package com.target.admin.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.target.admin.request.AddProductRequest;
import com.target.admin.request.CashRequest;
import com.target.admin.request.ChangeQuantityRequest;
import com.target.admin.request.SetPriceRequest;
import com.target.admin.response.SnapshotResponse;
import com.target.admin.service.ISupplierService;
import com.target.exceptions.VendingMachineException;
import com.target.model.Cash;
import com.target.model.Inventory;
import com.target.model.Product;
import com.target.model.response.ServiceResponse;
import com.target.utils.AppUtil;
import com.target.utils.ResponseUtil;
import com.target.validator.SupplierValidator;

public class SupplierServiceImpl implements ISupplierService {

	SupplierValidator validator = new SupplierValidator();

	@Override
	public ServiceResponse addProductToInventory(AddProductRequest request, Inventory inventory) {
		Product product = request.getProduct();
		try {
			validator.validateProduct(product);
			inventory.getProducts().put(AppUtil.getUid(), product);
			return ResponseUtil.getSuccessResponse("Product Added Successfully");
		} finally {
			
		}
	}

	@Override
	public ServiceResponse changeQuantity(ChangeQuantityRequest request, Inventory inventory) {
		try {
			validator.validateInteger(request.getQuantity());
			inventory.getProducts().get(request.getProductId()).setQuantity(request.getQuantity());
			return ResponseUtil.getSuccessResponse("Quantity updated successfully");
		} catch (VendingMachineException e) {
			return ResponseUtil.getFailureResponse(e.getMessage(), e.getErrorCode());
		}
	}

	@Override
	public ServiceResponse setPrice(SetPriceRequest request, Inventory inventory) {
		try {
			validator.validateInteger(request.getPrice());
			inventory.getProducts().get(request.getProductId()).setPrice(request.getPrice());
			return ResponseUtil.getSuccessResponse("Price updated successfully");
		} catch (VendingMachineException e) {
			return ResponseUtil.getFailureResponse(e.getMessage(), e.getErrorCode());
		}
	}

	@Override
	public SnapshotResponse collectCash(CashRequest request, List<Cash> cashList) {
		SnapshotResponse response = new SnapshotResponse();
		for (Cash cash: request.getCashList()) {
			
			cashList.remove(cash);
		}
		response.setCashList(cashList);
		response.setMessage("Cash collected successfully!");
		response.setSuccess(true);
		response.setTotalCash(AppUtil.getTotalCash(cashList));
		return response;
	}

	@Override
	public SnapshotResponse addCash(CashRequest request, List<Cash> cashList) {
		SnapshotResponse response = new SnapshotResponse();
		cashList.addAll(request.getCashList());
		response.setCashList(cashList);
		response.setMessage("Cash added successfully!");
		response.setSuccess(true);
		response.setTotalCash(AppUtil.getTotalCash(cashList));
		return response;
	}

	@Override
	public ServiceResponse resetCash(List<Cash> cashList) {
		cashList = new ArrayList<>();
		return ResponseUtil.getSuccessResponse("Cash Reset success!");
	}

	@Override
	public ServiceResponse resetInventory(Inventory inventory) {
		inventory = new Inventory();
		return ResponseUtil.getSuccessResponse("Inventory reset success!");
	}

}
