package com.target;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.target.admin.request.AddProductRequest;
import com.target.admin.service.ISupplierService;
import com.target.admin.service.impl.SupplierServiceImpl;
import com.target.model.Cash;
import com.target.model.Inventory;
import com.target.model.Product;
import com.target.model.response.ServiceResponse;
import com.target.model.type.Denomination;
import com.target.user.service.IConsumerService;
import com.target.user.service.impl.ConsumerServiceImpl;
import com.target.utils.AppUtil;

public class VendingMachine {
	
	public static final Integer SUPPLIER_USER = 1;
	public static final Integer CONSUMER_USER = 2;
	
	public static void main(String[] args) {
		//Create inventory
		Product prodA = new Product("prodA", 10, 5);
		Product prodB = new Product("prodB", 15, 0);
		Product prodC = new Product("prodC", 20, 2);
		
		Inventory inventory = new Inventory();
		
		Map<String, Product> productMap = new HashMap<>();
		productMap.put(AppUtil.getUid(), prodA);
		productMap.put(AppUtil.getUid(), prodB);
		productMap.put(AppUtil.getUid(), prodC);
		inventory.setProducts(productMap);
	
		List<Cash> cash = new ArrayList<>();
		cash.add(new Cash(Denomination.TEN, 1));
		cash.add(new Cash(Denomination.FIVE, 0));
		cash.add(new Cash(Denomination.TWENTY, 5));
		
		Integer userOption = null;
		
		while(true) {
			System.out.println("User Type: \n 1. Supplier \n 2. Consumer");
			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				userOption = Integer.parseInt(br.readLine());
			} catch (IOException e) {
				
			}
			if (userOption != null && (userOption == SUPPLIER_USER || userOption == CONSUMER_USER)) {
				break;
			} else {
				System.out.println("Invalid input. Please try again!");
			}
		}
		
		switch(userOption) {
		case 1:
			ISupplierService supplier = new SupplierServiceImpl();
			loginSupplier(supplier, inventory);
			break;
		case 2:
			IConsumerService consumer = new ConsumerServiceImpl();
			break;
		}
		
		
	}

	private static ServiceResponse loginSupplier(ISupplierService supplier, Inventory inventory) {
		String password = "target";
		while (true) {
			System.out.println("Enter password:");
			String inputPassword = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				inputPassword = br.readLine();
			} catch (IOException e) {
				
			}
			if (inputPassword.equals(password)) {
				break;
			} else {
				System.out.println("Invalid password. Please try again!");
			}
		}
		
		Integer input = null;
		while(true) {
			System.out.println("Select an option: \n 1. Add Product, 2. Change Quantity, 3. Set Prices");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				input = Integer.parseInt(br.readLine());
			} catch (IOException e) {
				
			}
			if (input >= 1 && input <= 3) {
				break;
			} else {
				System.out.println("Invalid input. Please try again!");
			}
		}
		while (true) {
			
			switch (input) {
			case 1:
				//Add product
				String productName = null;
				Integer quantity = null;
				Integer price = null;
				System.out.println("Product Name:");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				try {
					productName = br.readLine();
				} catch (IOException e) {
					
				}
				System.out.println("Quantity:");
				br = new BufferedReader(new InputStreamReader(System.in));
				try {
					quantity = Integer.parseInt(br.readLine());
				} catch (IOException e) {
					
				}
				System.out.println("Price:");
				br = new BufferedReader(new InputStreamReader(System.in));
				try {
					price = Integer.parseInt(br.readLine());
				} catch (IOException e) {
					
				}
				Product product = new Product(productName, price, quantity);
				AddProductRequest request = new AddProductRequest();
				request.setProduct(product);
				supplier.addProductToInventory(request, inventory);
				System.out.println("Inventory:" + inventory.toString());
			}
			
		}
		
	}
}
