package com.target;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
/**
 * Created by shruti.mehrotra on 16/5/16.
 */
@SpringBootApplication
@ImportResource("classpath:spring/application-context.xml")
@ComponentScan({"com.target"})
@EnableBatchProcessing
public class App {

   public static void main(String[] args) {
      SpringApplication.run(App.class, args);
      System.out.println("Application started!");
      
   }

}
