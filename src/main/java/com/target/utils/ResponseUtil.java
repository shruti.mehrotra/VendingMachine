package com.target.utils;

import com.target.model.response.ServiceResponse;
import com.target.model.type.ErrorCode;

public class ResponseUtil {
	public static ServiceResponse getSuccessResponse(String message) {
		ServiceResponse response = new ServiceResponse();
		response.setMessage(message);
		response.setSuccess(true);
		return response;
	}
	
	public static ServiceResponse getFailureResponse(String message, ErrorCode errorCode) {
		ServiceResponse response = new ServiceResponse();
		response.setErrorCode(errorCode);
		response.setMessage(message);
		response.setSuccess(true);
		return response;
	}
}
