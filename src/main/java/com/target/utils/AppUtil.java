package com.target.utils;

import java.util.List;
import java.util.UUID;

import com.target.model.Cash;

public class AppUtil {
	public static String getUid() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
	public static Long getTotalCash(List<Cash> cashList) {
		Long totalCash = 0L;
		for (Cash cash: cashList) {
			totalCash += cash.getDenomination().getDenomination() * cash.getQuantity();
		}
		return totalCash;
	}
}
