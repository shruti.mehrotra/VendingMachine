package com.target.validator;

import org.springframework.util.StringUtils;

import com.target.exceptions.VendingMachineException;
import com.target.model.Product;
import com.target.model.type.ErrorCode;

public class SupplierValidator {
	public void validateProduct(Product product) {
		if (product.getPrice() != null && product.getPrice() > 0 && product.getQuantity() != null && product.getQuantity() > 0 && !StringUtils.isEmpty(product.getProductName())) {
			
		} else {
			throw new VendingMachineException("Invalid product!" , ErrorCode.INVALID_PRODUCT);
		}
	}
	
	public void validateInteger(Integer value) {
		if (value != null && value > 0) {
			
		} else {
			throw new VendingMachineException("Invalid value!" , ErrorCode.INVALID_INT_VALUE);
		}
	}
}
