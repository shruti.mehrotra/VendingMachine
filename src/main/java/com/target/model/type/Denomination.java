package com.target.model.type;

import lombok.Getter;
import lombok.Setter;

public enum Denomination {
	
	FIVE(5), TEN(10), TWENTY(20);
	
	@Getter
	@Setter
	private Integer denomination;
	
	private Denomination(Integer denomination) {
		this.denomination = denomination;
	}
}
