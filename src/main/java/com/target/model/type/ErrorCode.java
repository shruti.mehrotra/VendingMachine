package com.target.model.type;

import lombok.Getter;
import lombok.Setter;

public enum ErrorCode {
	INVALID_DENOMINATION("VM_100"), INVALID_PRODUCT("VM_101"), INVALID_INT_VALUE("VM_102");
	
	
	@Getter
	@Setter
	private String errorCode;

	private ErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
