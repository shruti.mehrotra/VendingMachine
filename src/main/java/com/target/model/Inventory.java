package com.target.model;

import java.util.Map;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Inventory {
	Map<String, Product> products;
}
