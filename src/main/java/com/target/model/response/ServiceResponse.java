package com.target.model.response;

import com.target.model.type.ErrorCode;

import lombok.Data;

@Data
public class ServiceResponse {
	private boolean isSuccess;
	private String message;
	private ErrorCode errorCode;
}
