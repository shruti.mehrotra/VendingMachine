package com.target.model;

import com.target.model.type.Denomination;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cash {
	private Denomination denomination;
	private Integer quantity;
}
