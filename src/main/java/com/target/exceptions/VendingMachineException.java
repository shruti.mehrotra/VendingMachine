package com.target.exceptions;

import com.target.model.type.ErrorCode;

import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 * 
 */
@NoArgsConstructor
public class VendingMachineException extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5590708342883757683L;
	private ErrorCode errorCode;
	private Class<? extends VendingMachineException> exceptionCause;

	public VendingMachineException withErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
		return this;

	}

	public VendingMachineException(Throwable e) {
		super(e);
	}

	public VendingMachineException(String message) {
		super(message);
	}
	
	public VendingMachineException(String message, ErrorCode errorCode) {
		super(message);
		setErrorCode(errorCode);
	}

	public VendingMachineException(String message, Throwable e) {
		super(message, e);
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Class<?> getExceptionCause() {
		return exceptionCause;
	}

	public void setExceptionCause(
			Class<? extends VendingMachineException> exceptionCause) {
		this.exceptionCause = exceptionCause;
	}

	{
		this.setExceptionCause(this.getClass());
	}

}